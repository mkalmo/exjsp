<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.LinkedHashMap" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">.even { color: red; } </style>
</head>
<body>

<%
  LinkedHashMap<String, String> oddMap = new LinkedHashMap<String, String>();
  oddMap.put("1", "üks");
  oddMap.put("2", "kolm");
  oddMap.put("3", "viis");
  oddMap.put("4", "seitse");

  LinkedHashMap<String, String> evenMap = new LinkedHashMap<String, String>();
  evenMap.put("1", "kaks");
  evenMap.put("2", "neli");
  evenMap.put("3", "kuus");
  evenMap.put("4", "kaheksa");

  pageContext.setAttribute("odd", oddMap);
  pageContext.setAttribute("even", evenMap);
%>

<form method="post" action=".">
  <select name="listName">
    <option value=""/>
    <!-- Kuvada valikud odd, even -->
  </select>

  <input type="submit" value="Värskenda">
</form>

<!-- Näidata valitud listi sisu välja -->

<c:forEach var="each" items="${pageScope['odd']}">

    <!-- ... -->

</c:forEach>

</body>
</html>